<?php

#XQR:xjurca07

/*
***
* Soubor: xqr.php
* verze: 0.8
* Změna: 16.3.2014
*
* Projekt: IPP XQR: XML Query; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 18.2.2014
* Kompilator: PHP 5.5.3-1ubuntu2.1 (cli)
***
*/

/**
* 	
*/
class Message	//trida
{

	/*
	#####################################
	#	0 - tisk na stdout, 1 netiske   #
	#####################################
	*/
	public $printStdoutStop = 1;

	/**
	*	Tisk na STDOUT/STDERR
	*/
	public function PrintMsg($priority, $message) //metoda
	{
		if ($priority == 2) 
		{
			// tisk chybovych hlasek
			file_put_contents('php://stderr', $message."\n");
		}
		else
		{
			if (!$this->printStdoutStop) 
			{
				file_put_contents('php://stdout', $message."\n");
			}
		}
	}

	public function TVarDump($src)
	{
		if (!$this->printStdoutStop) 
		{
			var_dump($src);
		}
	}

	/*
	*	Napoveda
	*/
	public function PrintHelp()	
	{
		echo ("Projekt: XML Query v PHP 5, projekt c.1 IPP\n".
			"============================================\n".
			"Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz\n".
			"Popis: Skript provadi vyhodnoceni zadaneho dotazu, ktery je podobny prikazu SELECT jazyka SQL\n".
			"       nad vstupem ve formatu XML. Vystupem je XML splnujici pozadavky zadaneho dotazu.\n".
			"       Parametry --query a -qf nelze kombinovat\n".
			"Parametry:\n".
			" --help\t\t\t•vytiskne napovedu\n".
			" --input=filename\t•vstupni soubor ve formatu XML\n".
			" --output=filename\t•vystupni soubor ve formatu XML\n".
			" --query='dotaz'\t•dotaz v dotazovacim jazyce\n".
			" --qf=filename\t\t•dotaz v dotazovacim jazyce definovan v externim souboru\n".
			" -n\t\t\t•nebude se generovat XML hlavicka ve vystup scriptu\n".
			" --root=element\t\t•jmeno paroveho korenoveho elementu\n");
	}	
}

/**
* 
*/
class Parametry
{
	public $input = null;
	public $outpu = null;
	public $query = null;
	public $qf = null;
	public $root = null;

	public $bool_input = 0;
	public $bool_output = 0;
	public $bool_qf = 0;
	public $bool_query = 0;
	public $bool_root = 0;
	public $bool_n = 0;
}

/**
* 
*/
class RegelerniVyraz
{
	public $regulerVyraz = null;
	public $zdrojakRV = null;
	public $patch = null;
	public $patchWhere = null;
}

/**
* 
*/
class HodnotyDotazu
{
	public $value = null;

	public $select = null;

	public $limit = 0;

	public $from = null;
	public $fromDot = null;
	public $fRoot = false;

	public $wNot = -1;
	public $contains = false;
	public $where = null;
	public $whereDot = null;
	public $operator = null;
	public $literal = null;

	public $bLimit = false;
	public $bfrom = false;
	public $bWhere = false;

	public $bContains = false;
	public $oper = null;
}

/**
* 
*/
class VstupVystup
{
	public $head = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	public $result = null;
	public $output = null;
	public $xml = null;
}


// Inicializace objektu
$PRMT = new Parametry();
$MSG = new Message();
$RV = new RegelerniVyraz();
$DATA = new HodnotyDotazu();
$IO = new VstupVystup();

// Regulerni vyraz, slouzi pro kontrolu dotazu
$RV->regulerVyraz = "^SELECT[[:space:]]+[a-zA-Z_]+[[:space:]]+(LIMIT[[:space:]]+[0-9]+[[:space:]]+)?FROM[[:space:]]*((ROOT|[a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)[[:space:]]*)?((WHERE[[:space:]]+(NOT[[:space:]]+)*([a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)([[:space:]]+CONTAINS[[:space:]]+(\"(.)+\"[[:space:]]*)|[[:space:]]*(=|<|>)[[:space:]]*([[:space:]]*[0-9.]+[[:space:]]*|\"(.)+\"[[:space:]]*)))[[:space:]]*)?[[:space:]]*[[:cntrl:]]*$";

/**
* ####  NETISKNOUT STDOUT 
*/
$MSG->printStdoutStop = 1;
$MSG->TVarDump($argv);

/*
#	Vypnuti chybovyh hlasek , vlastni rezie
*/
error_reporting(0);


if ($argc < 2 && $argc > 6)
{
	$MSG->PrintMsg(2, "Zadan chybny pocet parametru. Pro napovedu zadejte: '".$argv[0]." --help'.");
	exit(1);
}
elseif ($argc == 2) 
{
	if ( in_array($argv[1], array('--help')) )
	{
		$MSG->PrintHelp();
		//echo "napoveda";
		exit(0);
	}
	else 
	{
		$MSG->PrintMsg(2, "Zadan chybny pocet parametru. Pro napovedu zadejte: '".$argv[0]." --help'.");
		exit(1);
	}
}
else
{
	foreach ($argv as $item)
	{
		if (strcmp($argv[0], $item) == 0) 
		{
			continue;
		}
		else
		{
			if(strncmp($item, "--input=", 8) == 0)
			{
				$PRMT->input = substr($item,8);
				if($PRMT->bool_input == 1)
				{
					$MSG->PrintMsg(2, "Error --input, parametr --input zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_input = 1;
				$MSG->PrintMsg(1, "input: ".$PRMT->input);

				if (!file_exists($PRMT->input))
				{
					$MSG->PrintMsg(2, "Error --input, soubor '".$PRMT->input."' neexistuje");
					exit(2);
				}

				if(!$IO->xml = simplexml_load_file($PRMT->input))
				{
					$MSG->PrintMsg(2, "Error --input, soubor '".$PRMT->input."' nelze otevrit.");
					exit(4);
				}
			}
			elseif (strncmp($item, "--output=", 9) == 0)
			{
				$PRMT->output = substr($item,9);
				if($PRMT->bool_output)
				{
					$MSG->PrintMsg(2, "Error --output, parametr --output zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_output = 1;
				$MSG->PrintMsg(1, "output: ".$PRMT->output);
			}
			elseif(strncmp($item, "--query=", 8) == 0)
			{
				if ($PRMT->bool_qf == 1)
				{
					$MSG->PrintMsg(2, "Error --query, parametr --query nelze kombinovat s --qf.");
					exit(1);
				}

				$PRMT->query = substr($item,8);
				if($PRMT->bool_query)
				{
					$MSG->PrintMsg(2, "Error --query, parametr --query zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_query = 1;
				$MSG->PrintMsg(2, "query: ".$PRMT->query);

				$RV->zdrojakRV = trim($PRMT->query);
				$RV->zdrojakRV = $PRMT->query;
			}
			elseif(strncmp($item, "--qf=", 5) == 0)
			{
				if ($PRMT->bool_query == 1)
				{
					$MSG->PrintMsg(2, "Error --qf, parametr --qf nelze kombinovat s --query.");
					exit(1);
				}

				$PRMT->qf = substr($item,5);
				if($PRMT->bool_qf)
				{
					$MSG->PrintMsg(2, "Error --qf, parametr --qf zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_qf = 1;
				$MSG->PrintMsg(1, "qf: ".$PRMT->qf);

				if (!file_exists($PRMT->qf))
				{
					$MSG->PrintMsg(2, "Error --qf, soubor '".$PRMT->qf."' neexistuje");
					exit(80);
				}

				if (!$RV->zdrojakRV = file_get_contents($PRMT->qf))
				{
					$MSG->PrintMsg(2, "Error --qf, soubor '".$PRMT->qf."' nelze otevrit.");
					exit(2);
				}
			}
			elseif(strncmp($item, "--root=", 7) == 0)
			{
				$PRMT->root = substr($item,7);
				if($PRMT->bool_root)
				{
					$MSG->PrintMsg(2, "Error --root, parametr --root zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_root = 1;
				$MSG->PrintMsg(1, "root: ".$PRMT->root);
			}
			elseif(strcmp($item, "-n") == 0)
			{
				if($PRMT->bool_n)
				{
					$MSG->PrintMsg(2, "Error -n, parametr -n zadan vicekrat.");
					exit(1);
				}
				$PRMT->bool_n = 1;
				$MSG->PrintMsg(1, "Je tam -n.");
			}
			else
			{
				$MSG->PrintMsg(1, "neco je blbe".$item);
				$MSG->PrintMsg(2, "Zadan chybny pocet parametru. Pro napovedu zadejte: '".$argv[0]." --help'.");
				exit(1);
			}
		}
	}

	if($PRMT->bool_input == 0)
	{
		$homepage = null;
		if (($homepage = file_get_contents('php://stdin')) == null){
			$MSG->PrintMsg(2, "Error stdin, soubor na stdion nelze nacist");
			exit(2);
		}

		if(!$IO->xml = simplexml_load_string($homepage))
		{
			$MSG->PrintMsg(2, "Error --input, soubor '".$PRMT->input."' nelze otevrit.");
			exit(2);
		}
	}
}


if (!$PRMT->bool_query && !$PRMT->bool_qf) {
	$MSG->PrintMsg(2, "Error argv, nezadan parametr --qf nebo --query.");
	exit(1);
}

/*
#	Overeni, jestli zadany dotaz splnuje podminky dotazovaciho jazyka
*/
if (!ereg($RV->regulerVyraz, $RV->zdrojakRV))
{
	$MSG->PrintMsg(2, "Error ereg, zadany dotaz nesplne podminky regulerniho vyrazu.\nDOTAZ: '".$RV->zdrojakRV."'");
	exit(80);
}

$MSG->PrintMsg(1, "Dotaz:".$RV->zdrojakRV);

/*
#
*/

$DATA->value = preg_split("[SELECT\s|\sLIMIT\s|\sFROM\s|\sWHERE\s|\sCONTAINS\s|=|<|>]", $RV->zdrojakRV);
$MSG->TVarDump($DATA->value);

/*
#
*/

$slider = 1;

if (preg_match(('/^SELECT\s/'), $RV->zdrojakRV))
{
	$DATA->select = trim($DATA->value[$slider]);
	$MSG->PrintMsg(1, "SELECT: ".$DATA->select);

	// Inkrementace posuvniku v poli pro LIMIT|FROM 
	$slider += 1;
}

if (preg_match(('/^SELECT\s+(\w+)\s+LIMIT\s/'), $RV->zdrojakRV))
{
	$DATA->limit = trim($DATA->value[$slider]);
	$DATA->bLimit = true;
	$MSG->PrintMsg(1, "LIMIT: ".$DATA->limit);
	$slider += 1;
}

if (preg_match(('/^SELECT\s+(\w+)\s+(LIMIT\s+(\d+)\s+)?FROM\s*/'), $RV->zdrojakRV))
{
	$DATA->bfrom = true;

	if (preg_match('/^\./', $DATA->value[$slider]))
	{
		$DATA->from = trim($DATA->value[$slider], '\.');
		$DATA->from = trim($DATA->from);
		$MSG->PrintMsg(1, "FROM: .from: .".$DATA->from);
	}
	elseif (preg_match('/\./', $DATA->value[$slider])) 
	{

		$frrom = preg_split('[\.]', $DATA->value[$slider]);
		$DATA->from = trim($frrom[0]);
		$DATA->fromDot = trim($frrom[1]);
		$MSG->PrintMsg(1, "FROM: ".$DATA->from.".".$DATA->fromDot);
	}
	elseif (preg_match(('/ROOT\s*/'), $DATA->value[$slider]))
	{
		$DATA->fRoot = true;
		$MSG->PrintMsg(1, "FROM: ROOT");
	}
	else
	{
		$DATA->from = trim($DATA->value[$slider]);
		$MSG->PrintMsg(1, "FROM: (element|empty): ".$DATA->from);
	}
	$slider += 1;	
}

if (preg_match(('/^SELECT\s+(\w+)\s+(LIMIT\s+(\d+)\s+)?FROM\s*((\w+|\.\w+|\w+\.\w+))\s*WHERE\s/'), $RV->zdrojakRV))
{
	$DATA->bWhere = true;

	if (preg_match(('/^SELECT\s+(\w+)\s+(LIMIT\s+(\d+)\s+)?FROM\s*(\w+)\s*WHERE\s+NOT\s/'), $RV->zdrojakRV))
	{
		$MSG->PrintMsg(1, "where: NOT");
		$DATA->wNot = 1;
		$DATA->value[$slider] = trim(substr($DATA->value[$slider], 3));
		$MSG->PrintMsg(1, "1:".$DATA->value[$slider]);
		$DATA->value[$slider] = "WHERE ".$DATA->value[$slider];
		while(preg_match(('/^WHERE\sNOT\s/'), $DATA->value[$slider])) 
		{
			$DATA->wNot += 1;
			$DATA->value[$slider] = trim(substr($DATA->value[$slider], 9));
			$MSG->PrintMsg(1, $DATA->wNot.":".$DATA->value[$slider]);
			$DATA->value[$slider] = "WHERE ".$DATA->value[$slider];
		}
		$DATA->value[$slider] = trim(substr($DATA->value[$slider], 5));
		$MSG->PrintMsg(1, "vystup:".$DATA->value[$slider]);
	}
	$DATA->value[$slider] = trim($DATA->value[$slider]);
	if (preg_match('/^\./', $DATA->value[$slider]))
	{
		$DATA->where = trim($DATA->value[$slider], '\.');
		$DATA->where = trim($DATA->where);
		$MSG->PrintMsg(1, "where: .where: .".$DATA->where);
		$slider += 1;
	}
	elseif (preg_match('/\./', $DATA->value[$slider])) 
	{

		$frrom = preg_split('[\.]', $DATA->value[$slider]);
		$DATA->where = trim($frrom[0]);
		$DATA->whereDot = trim($frrom[1]);
		$MSG->PrintMsg(1, "WHERE: ".$DATA->where.".".$DATA->whereDot);
		$slider += 1;
	}
	else
	{
		$DATA->where = trim($DATA->value[$slider]);
		$MSG->PrintMsg(1, "WHERE: (element|empty): ".$DATA->where);
		$slider += 1;
	}

	if (preg_match(('/^SELECT\s+(\w+)\s+(LIMIT\s+(\d+)\s+)?FROM\s*((\w+|\.\w+|\w+\.\w+))\s*WHERE\s+(NOT\s+)*(\w+|\.\w+|\w+\.\w+)\s+(CONTAINS\s|=\s*|<\s*|>\s*)/'), $RV->zdrojakRV))
	{
		if (preg_match(('/^SELECT\s+(\w+)\s+(LIMIT\s+(\d+)\s+)?FROM\s*((\w+|\.\w+|\w+\.\w+))\s*WHERE\s+(NOT\s+)*(\w+|\.\w+|\w+\.\w+)\s+CONTAINS\s+/'), $RV->zdrojakRV)) {

			$DATA->bContains = true;
			$MSG->PrintMsg(1, "WHERE: CONTAINS");
		}

		if (preg_match(('/=/'), $RV->zdrojakRV)) 
			$DATA->oper .= "=";
		if (preg_match(('/</'), $RV->zdrojakRV))
			$DATA->oper .= "<";
		if (preg_match(('/>/'), $RV->zdrojakRV))
			$DATA->oper .= ">";

		if ($DATA->wNot != -1)
		{
			$DATA->wNot = $DATA->wNot % 2;
			if ($DATA->wNot == 1)
			{
				if (preg_match(('/=/'), $RV->zdrojakRV)) 
					$DATA->oper = "!=";
				if (preg_match(('/</'), $RV->zdrojakRV))
					$DATA->oper = ">";
				if (preg_match(('/>/'), $RV->zdrojakRV))
					$DATA->oper = "<";
			}
		}
	}

	// ulozeni literalu 
	$DATA->literal = trim($DATA->value[$slider]);
	$DATA->literal = trim($DATA->literal, '\"');
	$DATA->literal = trim($DATA->literal);
	$MSG->PrintMsg(1, "literal: ".$DATA->literal);
	
}

$slider = 2;
if ($DATA->bLimit)
	$slider += 1;

// FROM
if ($DATA->bfrom)
{
	if (preg_match('/^\./', $DATA->value[$slider]))
	{
		$RV->patch = $RV->patch."//*[@".$DATA->from."]"."[1]";
	}
	elseif (preg_match('/\./', $DATA->value[$slider])) 
	{
		$RV->patch = $RV->patch."//".$DATA->from;
		$RV->patch = $RV->patch."[@".$DATA->fromDot."]"."[1]";
	}
	elseif ($DATA->fRoot)
	{
		$RV->patch = $RV->patch."//*[1]";
	}
	else
	{
		$RV->patch = $RV->patch."//".$DATA->from."[1]";
	}
	$slider += 1;	
}

//	SELECT
if (preg_match(('/^SELECT\s/'), $RV->zdrojakRV))
{
	$RV->patch = $RV->patch."//".$DATA->select;
}

//	WHERE

if ($DATA->bWhere)
{
	$patchoun = $RV->patch."[count(.//";

	if ($DATA->bContains)
	{
		$atribut = 0;

		if ($DATA->wNot == 1)
			$RV->patchWhere .= "[not(";
		else
			$RV->patchWhere .= "[";

		$patchAtribut = $RV->patchWhere;

		$DATA->value[$slider] = trim($DATA->value[$slider]);		

		if (preg_match('/^\./', $DATA->value[$slider]))
		{	
			$RV->patchWhere = $RV->patchWhere."contains(@".$DATA->where.", '".$DATA->literal."')";
			$atribut = 1;
			$patchAtribut .= "*[contains(@".$DATA->where.", '".$DATA->literal."')]";
		}
		elseif (preg_match('/\./', $DATA->value[$slider])) 
		{
			$RV->patchWhere = $RV->patchWhere.$DATA->where."[contains(@".$DATA->whereDot.", '".$DATA->literal."')]";
		}
		else
		{
			$RV->patchWhere = $RV->patchWhere."contains(.//".$DATA->where."[1], '".$DATA->literal."')";
			$patchoun .= $DATA->where.")]";
		}

		if ($DATA->wNot == 1)
		{
			$RV->patchWhere .= ")]";
			$patchAtribut .= ")]";
		}
		else
		{
			$RV->patchWhere .= "]";
			$patchAtribut .= "]";
		}

		if ($atribut == 1)
		{
			$resAtribut = $IO->xml->xpath($RV->patch.$RV->patchWhere);
			$konec = null;
			foreach ($resAtribut as $hodnota ) 
			{
				$konec = $konec.$hodnota->asXML();
			}
			if (is_null($konec))
				$RV->patchWhere = $patchAtribut;
		}

	}
	else
	{
		$DATA->value[$slider] = trim($DATA->value[$slider]);
		if (preg_match('/^\./', $DATA->value[$slider]))
		{
			$MSG->PrintMsg(1, "where: .where: .".$DATA->where);
			$RV->patchWhere = $RV->patchWhere."[@".$DATA->where;
		}
		elseif (preg_match('/\./', $DATA->value[$slider])) 
		{
			$MSG->PrintMsg(1, "WHERE: ".$DATA->where.".".$DATA->whereDot);
			$RV->patchWhere = $RV->patchWhere."[".$DATA->where."[@".$DATA->whereDot;
		}
		else
		{
			$MSG->PrintMsg(1, "WHERE: (element|empty): ".$DATA->where);
			$RV->patchWhere = $RV->patchWhere."[".$DATA->where;
		}
		// zpracovani operatoru
		$RV->patchWhere = $RV->patchWhere.$DATA->oper."'".$DATA->literal."']";	
	}
}


if (($IO->xml->xpath($patchoun)) != 0)
	exit(4);

$RV->patch = $RV->patch.$RV->patchWhere;

$IO->result = $IO->xml->xpath($RV->patch);


if (preg_match(('/FROM\s+WHERE\s/'), $RV->zdrojakRV))
{
	$IO->result = null;
}

if (preg_match(('/FROM\s*$/'), $RV->zdrojakRV))
{
	$IO->result = null;
}

// tiknuti hlavicky xml souboru podle parametru -n
if($PRMT->bool_n == 0)
	$IO->output = $IO->output.$IO->head;

if (($DATA->limit != 0) || (!$DATA->bLimit))
{

	if($PRMT->bool_root == 1)
	$IO->output =$IO->output."<".$PRMT->root.">"; 

	$counter = 0;
	foreach ($IO->result as $key ) 
	{
		if ($DATA->bLimit)
		{
			if ($counter == $DATA->limit)
				break;
		}

		$IO->output = $IO->output.$key->asXML();
		$counter++;
	}

	if($PRMT->bool_root == 1)
		$IO->output =$IO->output."</".$PRMT->root.">"; 

	if (!is_null($IO->output))
		$IO->output = $IO->output."\n";

}
else
{
	if($PRMT->bool_root == 1)
	$IO->output =$IO->output."<".$PRMT->root."/>\n";
}

if ($PRMT->bool_output == 1)
{
	if (($descriptor = fopen($PRMT->output, 'w')) == null)
	{
		$MSG->PrintMsg(2, "Error --output, soubor nelze vytvorit" );
		exit(3);
	}

	file_put_contents($PRMT->output,$IO->output);

	fclose($descriptor);

}
else
	file_put_contents('php://stdout', $IO->output);

echo ("\n".$RV->patch."\n");
exit (0);

/*
TODO:
		* za where dat jednicky [1]
		* udelat aby select a where mohly byt stejny 
		* u where před element narva.//
		* udelat aby query nebylo bez uvizovek 

		--query=" ekjfntnlgn  wrehe 'litera' "


		SELECT book FROM library WHERE  df.id CONTAINS "bk102" 
*/
?>
