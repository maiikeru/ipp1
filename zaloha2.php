<?php

/*
***
* Soubor: xqr.php
* verze: 0.3
* Změna: 28.2.2014
*
* Projekt: IPP XQR: XML Query; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 18.2.2014
* Kompilator: PHP 5.5.3-1ubuntu2.1 (cli)
***
*/

/**
* 	
*/
class MyClass	//trida
{
	/*
	*	Napoveda
	*/
	public function PrintHelp()	//metoda
	{
		echo "Projekt: XML Query v PHP 5, projekt c.1 IPP\n";
		echo "Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz\n";
		echo "Popis:Skript provadi vyhodnoceni zadaneho dotazu, ktery je podobny prikazu SELECT jazyka SQL\n";
		echo "      nad vstupem ve formatu XML. Vystupem je XML splnujici pozadavky zadaneho dotazu.\n";
		echo "      Parametry --query a -qf nelze kombinovat\n";
		echo "Parametry:\n";
		echo " --help\t\t\t•vytiskne napovedu\n";
		echo " --input=filename\t•vstupni soubor ve formatu XML\n";
		echo " --output=filename\t•vystupni soubor ve formatu XML\n";
		echo " --query=dotaz\t\t•dotaz v dotazovacim jazyce\n";
		echo " --qf=filename\t\t•dotaz v dotazovacim jazyce definovan v externim souboru\n";
		echo " -n\t\t\t•nebude se generovat XML hlavicka ve vystup scriptu\n";
		echo " --root=element\t\t•jmeno paroveho korenoveho elementu\n";
	}

	public $input;
	public $outpu;
	public $query;
	public $qf;
	public $root;
	public $bool_input = 0;
	public $bool_output = 0;
	public $bool_qf = 0;
	public $bool_query = 0;
	public $bool_root = 0;
	public $bool_n = 0;

}
print_r($argv);

// Inicializace objektu
$Parametry = new MyClass();

// Regulerni vyraz, slouzi pro kontrolu dotazu
//$regulerVyraz = "^SELECT[[:space:]]+[a-zA-Z]+[[:space:]]+(LIMIT[[:space:]]+[0-9]+[[:space:]]+){0,1}FROM[[:space:]]+((ROOT|[a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)[[:space:]]+)?[[:cntrl:]]*$";
//$regulerVyraz = "^SELECT[[:space:]]+[a-zA-Z]+[[:space:]]+(LIMIT[[:space:]]+[0-9]+[[:space:]]+){0,1}FROM[[:space:]]+((ROOT|[a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)[[:space:]]+)?[[:cntrl:]]*$";
//$regulerVyraz = "^SELECT[[:space:]]+[a-zA-Z]+[[:space:]]+(LIMIT[[:space:]]+[0-9]+[[:space:]]+){0,1}FROM[[:space:]]+((ROOT|[a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)[[:space:]]+)?[[:cntrl:]]*$";
$regulerVyraz = "^SELECT[[:space:]]+[a-zA-Z]+[[:space:]]+(LIMIT[[:space:]]+[0-9]+[[:space:]]+){0,1}FROM[[:space:]]+((ROOT|[a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+)[[:space:]]+)?((WHERE[[:space:]]+(NOT[[:space:]]+)?([a-zA-Z]+|[a-zA-Z]+\.[a-zA-Z]+|\.[a-zA-Z]+))([[:space:]]+CONTAINS[[:space:]]+|[[:space:]]*(=|<|>)[[:space:]]*)([0-9]+|\"[a-zA-Z]+[0-9]+\"))?[[:space:]]*[[:cntrl:]]*$";

if ($argc == 1)
{
	fputs(STDERR, "Nezadany parametry. Pro napovedu zadejte '$argv[0] --help'\n");
	exit();
}
elseif ($argc == 2 && in_array($argv[1], array('--help')))
{
	$Parametry->PrintHelp();
	exit();
}
else
{
	foreach ($argv as $value)
	{
		if (strcmp($argv[0], $value) == 0) 
		{
			continue;
		}
		else
		{
			if(strncmp($value, "--input=", 8) == 0)
			{
				$Parametry->input = substr($value,8);
				$Parametry->bool_input = 1;
				echo "Parametr input: $Parametry->input\n";//delete

				if (!file_exists($Parametry->input))
				{
					fputs(STDERR, "Chyba --input, soubor '$Parametry->input' neexistuje.\n");
					exit();
				}

				if(!$xml = simplexml_load_file($Parametry->input))
				{
					fputs(STDERR, "Chyba --input, soubor '$Parametry->input' nelze otevrit.\n");
					exit();
				}


			}
			elseif (strncmp($value, "--output=", 9) == 0) 
			{
				$Parametry->output = substr($value,9);
				$Parametry->bool_output = 1;
				echo "Parametr output: $Parametry->output\n";//delete
			}
			elseif(strncmp($value, "--query=", 8) == 0)
			{
				if ($Parametry->bool_qf == 1)
				{
					fputs(STDERR, "Chyba, nelze kombinovat s parametrem --qf\n");
					exit();
				}

				$Parametry->bool_query = 1;
				$Parametry->query = substr($value,8);

				echo "Parametr query: $Parametry->query\n";//delete

				// testovat na regurelni vyraz

				$zdrojakRV = $Parametry->query;

				$zdrojakRV=trim($zdrojakRV);

				if (ereg($regulerVyraz, $zdrojakRV))
					echo "\nRV: OK\n";
				else
				{
					echo "\nRV: WRONG\n";
					fputs(STDERR, "Chyba --query, parametr '$Parametry->qf' zadan jako chybny regulerni vyraz.\n");
					exit();
				}

			}
			elseif(strncmp($value, "--qf=", 5) == 0)
			{
				if ($Parametry->bool_query == 1)
				{
					fputs(STDERR, "Chyba, nelze kombinovat s parametrem --query\n");
					exit();
				}

				$Parametry->bool_qf = 1;
				$Parametry->qf = substr($value,5);

				if (!file_exists($Parametry->qf))
				{
					fputs(STDERR, "Chyba --qf, soubor '$Parametry->qf' neexistuje.\n");
					exit();
				}

				if (!$zdrojakRV = file_get_contents($Parametry->qf))
				{
					fputs(STDERR, "Chyba --qf, soubor '$Parametry->qf' nelze otevrit.\n");
					exit();
				}

				if (ereg($regulerVyraz, $zdrojakRV))
					echo "\nRV: OK\n";
				else
				{
					echo "\nRV: WRONG\n";
					fputs(STDERR, "Chyba --qf, soubor '$Parametry->qf' zadan chybny regulerni vyraz.\n");
					exit();
				}
			}
			elseif(strncmp($value, "--root=", 7) == 0)
			{
				$Parametry->root = substr($value,7);
				$Parametry->bool_root = 1;

				echo "Parametr root: $Parametry->root\n";//delete
			}
			elseif(strcmp($value, "-n"))
			{
				$Parametry->bool_n = 1;
			}
		}
	} 
}

if (($Parametry->bool_input == 0) | ($Parametry->bool_output == 0) )
{
	echo "nezadan vstup vystup\n";
	exit();
}

if ($Parametry->bool_query == 0 && $Parametry->bool_qf == 0 )
{
	echo "nezadan dotaz\n";
	exit();
}

print_r("zdrojakRV: ".$zdrojakRV."\n");
	


$select = preg_split("[(SELECT\s|\sLIMIT\s)|\sFROM\s|\sWHERE\s|\sCONTAINS\s|=|<|>]", $zdrojakRV);

echo "\nzdrojak: $zdrojakRV\n";

print_r($select);





$num_from = 1;

if (preg_match(('/^SELECT\s/'), $zdrojakRV))
{
	echo "\nSELECTT element: $select[$num_from]\n";
	// Inkrementace posuvniku v poli pro FROM
	$num_from += 1;
}
else
echo "bla bla vla ";

/*Rozpracovani parametru LIMIT*/
if (preg_match(('/LIMIT\s/'), $zdrojakRV))
{
	$number_LIMIT = $select[$num_from];
	// Inkrementace posuvniku v poli pro FROM
	$num_from += 1;

	echo "LIMIT cislo: $number_LIMIT";
}

/*Rozpracovani parametru FROM*/
if (preg_match(('/FROM\s/'), $zdrojakRV))
{
	echo "\nFROM ";

	if (preg_match('/^\./', $select[$num_from]))
	{
		echo "\je tam prvni tecka $select[$num_from]\n";
		$trimed = trim($select[$num_from], '\.');
		echo "trimmed: $trimed\n";
	}
	elseif (preg_match('/\./', $select[$num_from])) 
	{
		echo "ve slove je tecka $select[$num_from]\n";

		$frrom = preg_split('[\.]', $select[$num_from]);
		print_r($frrom);
	}
	elseif (preg_match(('/ROOT\s/'), $select[$num_from]))
	{
		echo "Je tam root: $select[$num_from]\n";
	}
	else
		echo "nejaky element nebo nic ????\n";	

	// Inkrementace posuvniku v poli pro FROM
	$num_from += 1;		
}

if (preg_match(('/WHERE\s/'), $zdrojakRV))
{
	echo "\nWHERE ";

	if (preg_match(('/NOT\s/'), $select[$num_from]))
	{
		echo "je tam NOT:";

		$select[$num_from] = trim($select[$num_from]);

		echo "$select[$num_from]";

		$kuhh = trim(substr($select[$num_from], 3)) ;
		echo "\nstring:$kuhh\n";

		// Inkrementace posuvniku v poli pro FROM
		$num_from += 1;
		
	}
	else
		echo " neni tam NOT";
}

/*
$patch = NULL;
echo($select[2]);
if (preg_match(('/ROOT\s/'), $zdrojakRV)){
echo "111111111111111";
	$patch = $patch."//*[1]";
}
else
echo "22222222222222222";

if (preg_match(('/SELECT\s/'), $zdrojakRV))
{
	$patch = $patch."//".$select[1];
	echo "33333333333333";
}
else
echo "4444444444444";

echo($patch."\n");
$result = $xml->xpath($patch);
//print_r($result);

*/

$patch = NULL;

$num_from = 2;

/*Rozpracovani parametru LIMIT*/
if (preg_match(('/LIMIT\s/'), $zdrojakRV))
{
	$num_from += 1;
}


if (preg_match(('/FROM\s/'), $zdrojakRV))
{

	if (preg_match('/^\./', $select[$num_from]))
	{
		//echo "\je tam prvni tecka $select[$num_from]\n";
		$trimed = trim($select[$num_from], '\.');
		echo "trimmed: $trimed\n";
		$trimed = trim($trimed);

		$patch = $patch."//*[@".$trimed."]"."[1]";


	}
	elseif (preg_match('/\./', $select[$num_from])) 
	{
		

		//$frrom = preg_split('[\.]', $select[$num_from]);
		print_r($frrom);
		$trimmmesa = trim($frrom[0]);
		$patch = $patch."//".$trimmmesa;

		$trimmmesa = trim($frrom[1]);
		$patch = $patch."[@".$trimmmesa."]"."[1]";

		echo("\npatct:".$patch."\n");

	}
	elseif (preg_match(('/ROOT\s/'), $select[$num_from]))
	{
		$patch = $patch."//*[1]";
		echo "som to";
	}
	else
	{
		echo "$select[$num_from]";
		$trimmmesa = trim($select[$num_from]);	
		$patch = $patch."//".$trimmmesa."[1]";
		echo "tear som to";
	}

	// Inkrementace posuvniku v poli pro FROM
	$num_from += 1;		
}

//SELECT title FROM library.my

if (preg_match(('/^SELECT\s/'), $zdrojakRV))
{
	$trimmmesa = trim($select[1]);
	$num_from += 1;
	$patch = $patch."//".$trimmmesa;
}


if (preg_match(('/WHERE\s/'), $zdrojakRV))
{
	if (preg_match(('/WHERE\sNOT\s/'), $zdrojakRV))
	{
		echo "\nje tam NOT krasds999999999999999999999999999999999999999\n";
		$num_from += 1;
	}


}



print_r($patch."lksnflkn\n");

//$patch = "//library[@my][1]//title";
//$patch = "//@my[1]//title";
//$patch = "//*[@my][1]//title";
//$patch = "//*[1]//book[price < 20]"; // test 8
//$patch = "//*[1]//book[contains(@id,2)]"; // test 9
//$patch = "//*[1]//book[contains(@id,2)]"; // test 9



// možna from element možná chyba proč ? .[1]

$result = $xml->xpath($patch);


//SELECT title FROM library.my




//fipe put contens (php://stdin, odwgnogn);

// potlaceni chybove hlasky , kdyz nelze otevrit soubor, rve php
//erorr_reporting
//get_opt



//print_r($result);



$vyslede = NULL;

$vyslede = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

$countak = 0;

foreach ($result as $key ) 
{
	//if ($countak == 2)
	//{
	//	break;
	//}
	$vyslede = $vyslede.$key->asXML();
	$countak++;
}
	echo "\n$vyslede\n";



file_put_contents($Parametry->output, $vyslede);

echo "\n";

?>